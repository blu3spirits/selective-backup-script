#!/usr/bin/bash
set -euo pipefail
IFS=$'\n\t'

# Read all devices into block_devices array
readarray -t block_devices< <(lsblk --noheadings --nodeps --output NAME)
# Empty array declarations
declare -a all_devices
declare -a valid_devices

# Get the udev information regarding each disk
for disk in "${block_devices[@]}"
do
    long_link="$(udevadm info --query=symlink --name="${disk}")"
    if [[ -z ${long_link} ]]
    then
        continue
    fi
    regex="disk\/by-id\/(ata-[a-zA-Z0-9_-]+)"
    if [[ ${long_link} =~ $regex ]]; then
        capture="${BASH_REMATCH[1]}"
        all_devices+=("${capture}")
    fi
done

# Form the valid devices array from the all devices array
# This is where you would put restrictions if you so desired
valid_devices+=("$(echo ${all_devices[@]} | tr ' ' '\n')")
# Custom date format
date=$(date +%Y%m%d-%H%M%S)
LOG="${date}.log"

# Select the SOURCE disk from valid_devices[]
echo "Select the SOURCE Disk"
select DISK_ID in $valid_devices;
do
    SOURCE="/dev/disk/by-id/${DISK_ID}"
    res="$(lsblk --nodeps "$SOURCE" --bytes --noheadings --output SIZE)"
    break
done
# Select your DEST disk from valid_devices[]
echo "Select the DEST Disk"
select DISK_ID in $valid_devices;
do
    DEST="/dev/disk/by-id/${DISK_ID}"
    res="$(lsblk --nodeps "$SOURCE" --bytes --noheadings --output SIZE)"
    break
done

if [[ "${SOURCE}" == "${DEST}" ]]
then
    echo "ERROR: Source and destination cannot be the same"
    exit 1
fi


# DO NOT EDIT THIS LINE
_DD='/usr/bin/ddrescue'

echo "=============="
echo ""
echo "SOURCE: ${SOURCE}"
echo "DEST  : ${DEST}"
echo "LOG   : ${LOG}"
echo ""
echo -n "Is this correct? y/N"
read -r confirm
confirm="${confirm,,}"
if [[ "${confirm}" != 'y' ]]
then
    echo "Exiting on user cancel"
    exit 0
fi

exit 100
sudo "${_DD}" --no-trim "${SOURCE}" "${DEST}/${date}.${req}.dd" "${LOG}"
echo -n "Run the DD again (corruption)? y/N "
# choose whether or not you want to run the script again
read -r switch
switch="${switch,,}"
if [[ "$switch" == 'y' ]]; then
    echo -e "Running a destructive DD"
    sudo "${_DD}" "${SOURCE}" "${DEST}/${date}.${req}.dd" "${LOG}"
else
    exit
fi
